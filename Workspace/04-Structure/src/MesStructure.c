#include "MesStructure.h"
#include "stdio.h"

void init_point(ptr_point p, int x, int y)
{
    p->x=x;
    p->y=y;
}

void init_cercle(ptr_cercle c, const ptr_point p, double rayon)
{
/*     c->centre.x=p->x;
    c->centre.y=p->y; */
    c->centre=*p;
    c->rayon=rayon;
}

void afficher_cercle(const ptr_cercle c)
{
    printf("Centre (%d,%d)\n",c->centre.x,c->centre.y);
	printf("Rayon (%f)\n",c->rayon);
}