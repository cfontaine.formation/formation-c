#include"liste.h"
#include <stdlib.h>
#include <stdio.h>

void ajouterElm(struct elm_liste **liste,int data )
{
    struct elm_liste *elm=malloc(sizeof(struct elm_liste));
    elm->data=data;
    if(liste==NULL){
        elm->next=NULL;
    }else
    {
        elm->next=*liste;
        
    }
    *liste=elm;
}

void afficher_liste(struct elm_liste *liste)
{
    struct elm_liste *ptr_eml=liste;
    while (ptr_eml!=NULL){
        printf("%d\t",ptr_eml->data);
        ptr_eml=ptr_eml->next;
    }
}

void effacer_liste(struct elm_liste **liste)
{
    struct elm_liste *ptr_eml=*liste;
    struct elm_liste *ptr_del;
    while (ptr_eml!=NULL){
        printf("effacer %d",ptr_eml->data);
        ptr_del=ptr_eml;
        ptr_eml=ptr_eml->next;
        free(ptr_del);
    }
    *liste=NULL;
}

