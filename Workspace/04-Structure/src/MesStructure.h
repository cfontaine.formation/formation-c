struct point
{
    int x;
    int y;
};
union test{
    int y;
    double z; 
};

struct mot
{
unsigned int a : 1;
unsigned int b : 3;
int c : 5;
int : 3;
unsigned int d : 3;
};

enum direction { NORD=90,EST=180, SUD, OUEST=320 };
struct cercle{
    double rayon;
    struct point centre;
};

typedef struct point* ptr_point;
typedef struct cercle* ptr_cercle;

/* void init_point(struct point *p, int x, int y);
void init_cercle(struct cercle *c, const struct point *p, double rayon);
void afficher_cercle(const struct cercle *c); */
void init_point(ptr_point p, int x, int y);
void init_cercle(ptr_cercle c, const ptr_point p, double rayon);
void afficher_cercle(const ptr_cercle c);

