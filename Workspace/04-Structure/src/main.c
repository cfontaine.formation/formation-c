#include <stdio.h>
#include <stdlib.h>
#include "MesStructure.h"
#include "liste.h"


void testPointeur2Pointeur(int v,int* ptr_v,int ** ptr_ptr);
int main(void)
{
	struct point p;
	p.x=0;
	p.y=1;

	struct point p2={1,1};
	printf("(%d,%d)",p2.x,p2.y);
	p=p2;
	printf("(%d,%d)",p.x,p.y);
	if(p.x==p2.x && p.y==p2.y){ /* erreur p==p2 impossible*/
		printf("Les points sont égaux\n");
	}

	struct point tab[5]={{0,0},{1,3},{2,5},{2,9},{0,3}};
	int i;
	for(i=0;i<5;i++)
	{
		printf("tab[%d]=(%d,%d)\n",i,tab[i].x,tab[i].y);
	}

	struct cercle c={2.0,{2,3}};
	printf("Centre (%d,%d)\n",c.centre.x,c.centre.y);
	printf("Rayon (%f)\n",c.rayon);
	/* struct test a; */
	struct point point1;
	init_point(&point1,2,7);

	struct cercle cercle1;
	init_cercle(&cercle1,&point1,3.0);
	afficher_cercle(&cercle1);

 	/* struct point* point2= malloc(sizeof(struct point)); */ 

	struct elm_liste* lst=NULL;
	ajouterElm(&lst,2);
	ajouterElm(&lst,3);
	ajouterElm(&lst,4);
	ajouterElm(&lst,6);
	ajouterElm(&lst,10);
	afficher_liste(lst);
	effacer_liste(&lst);
	int val=3;
	int *ptri1=NULL;
	int*ptri2=NULL;
	testPointeur2Pointeur(val,ptri1,&ptri2);

	union test t;
 	 t.y=23;
	t.z=50000.6;
	
	printf("%d %f",t.y,t.z);

	struct mot wo;
	wo.a=1;
	wo.b=2;
	wo.c=~3+1;
	wo.d=0;
	printf("%x %x %x %d %x",wo.a,wo.b,wo.c,wo.c,wo.d);
	enum direction dir=SUD;
	printf("\n%d %d %d",dir,dir==NORD,dir==SUD);
	switch(dir)
	{
			case NORD:
			printf("NORD");
			break;
			case SUD:
			printf("SUD");
			break;
			default:
			printf("AUTRE");
	}
	return 0;
}

void testPorteStructure(){
	struct test{
		int a;
		double b;
	}t1,t2;
	struct test t3;
}

void testPointeur2Pointeur(int v,int* ptr_v,int ** ptr_ptr )
{
	v=12;
	int *p=malloc(sizeof(int));
	ptr_v=p;
	*ptr_ptr=p;
}