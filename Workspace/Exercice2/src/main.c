#include <stdio.h>
#include "tableau.h"

int main()
{
	int *tab = NULL;
	int size;
	int choix = 1;
	while (choix)
	{
		printf("choix ");
		scanf("%d", &choix);
		switch (choix)
		{
		case 1:
			if (tab)
			{
				destroyTab(tab);
			}
			size = saisieTaille();
			tab = createTab(size);
			if (tab)
			{
				saisieTab(tab, size);
			}
			else
			{
				printf("erreur de création du tableau\n");
			}
			break;
		case 2:
			if (tab)
			{
				afficher(tab, size);
			}
			break;
		case 3:
			if (tab)
			{
				printf("minimum=%d, maximum=%d, moyenne=%f\n",minimum(tab,size),maximum(tab,size),moyenne(tab,size));
			}
			break;
		case 4:
			if (tab)
			{
				sort(tab,size);
			}
			break;
		}
	}
	destroyTab(tab);
}
