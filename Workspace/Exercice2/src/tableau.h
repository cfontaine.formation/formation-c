
void afficher(int tab[],int size_tab);

int saisieTaille();

void saisieTab(int tab[],int size);

int* createTab(int size);

void destroyTab(int *tab);

int minimum(int tab[],int size);

int maximum(int tab[],int size);

double moyenne(int tab[],int size);

void sort(int tab[], int size_tab);

