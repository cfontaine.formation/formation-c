#include <stdio.h>
#include <stdlib.h>
#include "tableau.h"

void afficher(int tab[], int size_tab)
{
	int i;
	printf("[ ");
	for (i = 0; i < size_tab; i++)
	{
		printf("%d ", tab[i]);
	}
	printf("]\n");
}

int saisieTaille()
{
	int size;
	do
	{
		printf("Entrer la taille du tableau ");
		scanf("%d", &size);
	} while (size < 0);
	return size;
}

void saisieTab(int tab[], int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		printf("tab[%d]=", i);
		scanf("%d", &tab[i]);
	}
}

int* createTab(int size)
{
	return malloc(sizeof(int) * size);
}

void destroyTab(int *tab)
{
	if (tab)
	{
		free(tab);
		tab = NULL;
	}
}

int minimum(int tab[], int size)
{
	int minimum = tab[0];
	int i;
	for (i = 1; i < size; i++)
	{
		if (tab[i] < minimum)
		{
			minimum = tab[i];
		}
	}
	return minimum;
}

int maximum(int tab[], int size)
{
	int maximum = tab[0];
	int i;
	for (i = 1; i < size; i++)
	{
		if (tab[i] > maximum)
		{
			maximum = tab[i];
		}
	}
	return maximum;
}

double moyenne(int tab[], int size)
{
	double somme = tab[0];
	int i;
	for (i = 1; i < size; i++)
	{
		somme += tab[i];
	}
	return somme / size;
}

void sort(int tab[], int size_tab)
{
	int loop = 1;
	int end=size_tab-1;
	while (loop)
	{
		loop = 0;
		int i;
		for (i = 0; i < end; i++)
		{
			if (tab[i] > tab[i + 1])
			{
				int tmp = tab[i];
				tab[i] = tab[i + 1];
				tab[i + 1] = tmp;
				loop = 1;
			}
		}
		end--;
	}
}
