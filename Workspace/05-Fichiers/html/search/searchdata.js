var indexSectionsWithContent =
{
  0: "aelmnpt",
  1: "p",
  2: "m",
  3: "elmt",
  4: "anp",
  5: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros"
};

