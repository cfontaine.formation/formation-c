/**
 * @file main.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2020-07-03
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <stdio.h>
#include <string.h>

#define TEST_MACRO(x,y) x>y?x:y

/**
 * @brief 
 * 
 */
struct personne{
	char nom[25];
	char prenom[25];
	int anne_naissance;
};

void ecritureTexte(const char *path);
void lectureTexte(const char *path);
void ecritureTexteFormat(const char *path);
void lectureTexteFormat(const char *path);
void ecritureBin(const char *path,struct personne* p);
void lectureBin(const char *path,struct personne* p);
void testSeek();

int main(void)
{
	/* 	ecritureTexte("c:/formation/testio/fic1.txt");
	lectureTexte("c:/formation/testio/fic1.txt"); */
/* 	ecritureTexteFormat("c:/formation/testio/fic2.txt");
	lectureTexteFormat("c:/formation/testio/fic2.txt");
	struct personne per,per2;
	strcpy(per.prenom,"Antoine");
	strcpy(per.nom,"Beretto");
	per.anne_naissance=1928;
	ecritureBin("c:/formation/testio/fic1.dat",&per);
	lectureBin("c:/formation/testio/fic1.dat",&per2);
	printf("%s %s %d", per2.prenom,per2.nom,per2.anne_naissance); */
	/*testSeek();*/
/* 	remove("c:/formation/testio/aeffacer.txt");
	remove("c:/formation/testio/reaeffacer/");
	printf("%d\n",remove("c:/formation/testio/effacer_erreur")); */
/* 	rename("c:/formation/testio/arenommer.txt","c:/formation/testio/renommer.txt");
	rename("c:/formation/testio/renommer.txt","c:/formation/deplacer.txt"); */
	#if __WIN32__
	printf("%s %d %s",__FILE__,__LINE__,__DATE__);
	#else
	printf("%s %d %s",__TIME__);
	#endif
	printf("%d",TEST_MACRO(3,4));

	printf("%d",TEST_MACRO(5,4));
	int i[5];
	i[10]=0;
	return 0;
}

void ecritureTexte(const char *path)
{
	FILE *file;
	file = fopen(path, "wt");
	if (file != NULL)
	{
		int i = 0;
		for (i = 0; i < 10; i++)
		{
			fputs("Hello world!\n", file);
		}
		fclose(file);
	}
}

void lectureTexte(const char *path)
{
	FILE *file;
	file = fopen(path, "rt");
	if (file != NULL)
	{
		char str[64];
		while (!feof(file))
		{
			if (fgets(str, 64, file))
				printf("%s", str);
		}
		fclose(file);
	}
}

void ecritureTexteFormat(const char *path)
{
	FILE *file;
	file = fopen(path, "wt");
	if (file != NULL)
	{
		int i = 0;
		for (i = 0; i < 10; i++)
		{
			fprintf(file,"%d  %s\n", i, "HelloWorld!");
		}
		fclose(file);
	}
}

void lectureTexteFormat(const char *path)
{
	FILE *file;
	file = fopen(path, "rt");
	if (file != NULL)
	{
		int ligne;
		char str[64];
		int nb_read=1;
		while (nb_read>0)
		{
			nb_read=fscanf(file, "%d  %s\n", &ligne,str);
			 if (nb_read>0)
			{ 
				printf("%d  %s\n", ligne, str);
		 	} 
		}
		fclose(file);
	}
}

/**
 * @brief écriture d'une personne dans un fichier 
 * 
 * @param path chemin 
 * @param p la personne
 */
void ecritureBin(const char *path,struct personne* p)
{
	FILE *file;
	file = fopen(path, "wb");
	if (file != NULL)
	{
		fwrite(p,sizeof(struct personne),1,file);
		printf("%ld",ftell(file));
		fclose(file);
	}
}

void lectureBin(const char *path,struct personne* p)
{
	FILE *file;
	file = fopen(path, "rb");
	if (file != NULL)
	{
		fread(p,sizeof(struct personne),1,file);
		fclose(file);
	}
}

void testSeek()
{
	FILE *file;
	file = fopen("c:/formation/testio/test1.dat", "w+b");
	if (file != NULL)
	{
		int i=0;
		for(i=0;i<15;i++){
			fwrite(&i,1,sizeof(int),file);
		}
		fseek(file,0,SEEK_SET);
		int val=0;
		for(i=0;i<5;i++)
		{
			fseek(file,sizeof(int),SEEK_CUR);
			fread(&val,sizeof(int),1,file);
			printf("%d",val);
		}
		fclose(file);
	}
}
