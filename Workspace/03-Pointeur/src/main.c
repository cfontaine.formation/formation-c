#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void testParamtab(double tab[], int size);

void testParamtab_ptr(double *ptr, int size);

void testParamtab2d(int tab[][2], int size);
int inferieur(int v1, int v2);
int superieur(int v1, int v2);
void sort(int tab[], int size_tab,int(*ptr_function)(int, int));
void afficher(int tab[], int size_tab);

int main(void)
{
	/* Pointeur */
	char c = 'A';
	char d = 'D';
	char *ptr = &c;
	printf("adresse contenu dans le pointeur %p\n", ptr);
	printf("adresse contenu dans le pointeur %d\n", *ptr);
	*ptr = 'Z';
	printf("%c\n", c);

	/*Pointeur Constant */

	const char *ptr_c1 = &c;
	printf("%c\n", *ptr_c1);
	/*ptr_c1='E';*/
	ptr_c1 = &d;

	char *const ptr_c2 = &c;
	printf("%c\n", *ptr_c2);
	*ptr_c2 = 'E';
	/*ptr_c2=&d;*/

	const char *const ptr_c3 = &c;
	printf("%c\n", *ptr_c3);
	/* 	*ptr_c3='E';
	ptr_c3=&d; */

	/* Affectation Pointeur*/
	int valeur = 1234;
	int *p1, *p2;
	p1 = &valeur;
	p2 = p1;
	printf("%n %n\n", p1, p2);
	p1 = NULL;
	c = 50;
	char *pc1 = &c;
	p1 = (int *)pc1;
	printf("%d\n", c);
	printf("%p %n\n", pc1, p1);
	printf("%d\n", *p1);

	/*Arithmétique des pointeurs*/
	double ap[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
	double *ptr_ap = ap; /* équivalant à &ap[0]*/
	printf("%p %f\n", ptr_ap, *ptr_ap);
	ptr_ap++;
	printf("%p %f\n", ptr_ap, *ptr_ap);
	int dec = 3;
	ptr_ap += dec;
	printf("%p %f\n", ptr_ap, *ptr_ap);

	ptr_ap--;
	printf("%p %f\n", ptr_ap, *ptr_ap);
	ptr_ap -= dec;
	double *ptr_start = &ap[0];
	double *ptr_end = &ap[5];
	printf("%d\n", ptr_end - ptr_start);
	printf("%d\n", ptr_start - ptr_end);

	int valeur2 = 100;
	valeur = 100;
	p1 = &valeur;
	p2 = &valeur2;
	if (p1 == p2)
	{
		printf("Les pointeurs sont égaux test1\n");
	}
	p1 = p2;
	if (p1 == p2)
	{
		printf("Les pointeurs sont égaux test2\n");
	}
	if (p1) /* p1 !=NULL*/
	{
		printf("Le pointeur p1 n'est pas égal à NULL\n");
	}
	p1 = NULL;
	if (!p1) /* p1 ==NULL*/
	{
		printf("Le pointeur p1 est égal à NULL\n");
	}

	ptr_ap = ap; /* équivalant à &ap[0]*/
	printf("%f\n", *ptr_ap);
	ptr_ap = ap + 1; /* équivalant à &ap[1]*/
	printf("%f\n", *ptr_ap);
	ptr_ap += 2; /* équivalant à &ap[3]*/
	printf("%f\n", *ptr_ap);
	ptr_ap = ap;
	printf("%f\n", *(ptr_ap + 3)); /* équivalant à ap[3]*/
	printf("%f\n", ptr_ap[3]);
	ptr_ap = ap + 4;			/* équivalant à ap[4]*/
	printf("%f\n", ptr_ap[-3]); /* équivalant à ap[1]*/

	/*Tableau à 2 dimensions*/
	int tab[3][2] = {{0, 1}, {2, 3}, {4, 5}};
	printf("%d\n", sizeof(tab) / sizeof(int));
	printf("%d\n", sizeof(tab + 0) / sizeof(int));
	testParamtab(ap, 6);
	testParamtab_ptr(ap, 6);
	testParamtab2d(tab, 3);

	/* Gestion dynamique de la mémoire */
	int *ptr_dyn = malloc(1 * sizeof(int));
	if (ptr_dyn)
	{
		*ptr_dyn = 345;
		printf("%d\n", *ptr_dyn);
		free(ptr_dyn);
	}
	
	int size = 6;
	double *ptr_double = malloc(size * sizeof(double));
	if (ptr_double)
	{
		int i;
		for (i = 0; i < size; i++)
		{
			ptr_double[i] = 0.0;
		}
		for (i = 0; i < size; i++)
		{
			printf("%f\n", ptr_double[i]);
		}
		free(ptr_double);
	}

	int (*ptr_fnc)(int,int); 
	ptr_fnc=superieur;
	printf("%d\n",ptr_fnc(4,5));
	ptr_fnc=inferieur;
	printf("%d\n",ptr_fnc(4,5));

	int tab_sort[]={4,7,3,0,-3,10,6};
	sort(tab_sort,7,superieur);
	afficher(tab_sort,7);
	sort(tab_sort,7,inferieur);
	afficher(tab_sort,7);

	/* chaine de caractères */

	char str[]="";
	char* str_const="azerty"  " qsdff";
	printf("%s\n",str);
	printf("%d\n",sizeof(str));
	printf("%s\n",str_const);

	printf("%d\n",strlen(str_const));

	char str1[10]="Hello";
	char str2[10]="World!";
/* 	strcat(str1,str2); */
/*	strncat(str1,str2,4);*/
	char str3[10]="Hello";
	char str4[10]="Helium";
	printf("%s %s\n",str1 , str2);
	printf("%d %d\n",strcmp(str3,str4) , strcmp(str1,str3));
	char *pos=strrchr(str_const,'e');
	printf("%d\n",pos-str_const);
	char* str_sub="ty";
	pos=strstr(str_const,str_sub);
	printf("%d\n",pos-str_const);
	printf("%d,%ld,%f",atoi("123"),atol("aaa"),atof("12.89"));
	return 0;
}

void testParamtab(double tab[], int size)
{
	printf("%f", tab[2]);
}

void testParamtab_ptr(double *ptr, int size)
{
	printf("%f", ptr[2]);
	printf("%f", *(ptr + 2));
}

void testParamtab2d(int tab[][2], int size)
{
	printf("%d", tab[1][1]);
}

void sort(int tab[], int size_tab,int(*ptr_function)(int, int))
{
	int loop = 1;
	int end=size_tab-1;
	while (loop)
	{
		loop = 0;
		int i;
		for (i = 0; i < end; i++)
		{
			if (ptr_function(tab[i],tab[i + 1]))
			{
				int tmp = tab[i];
				tab[i] = tab[i + 1];
				tab[i + 1] = tmp;
				loop = 1;
			}
		}
		end--;
	}
}

int superieur(int v1, int v2)
{
	return v1>v2;
}

int inferieur(int v1, int v2)
{
	return v1<v2;
}

void afficher(int tab[], int size_tab)
{
	int i;
	printf("[ ");
	for (i = 0; i < size_tab; i++)
	{
		printf("%d ", tab[i]);
	}
	printf("]\n");
}