#include <stdio.h>

#define MAXIMUM 120
#define LUNDI 1

int main(void)
{
	printf("Hello World!");
	/* Déclaration Variable */
	int i;
	/* Déclaration multiple de Variable */
	double hauteur, largeur;
	/* Initialisation de variable */
	i = 12;
	hauteur = 100.0;
	largeur = 1e3;
	/* déclaration et initialisation */
	int j = 42, k = 67;
	printf("valeur =%d ,  %f , %+6.2f, %04d\n", i, hauteur,largeur, j);

	/* Littéraux entier changement de base */
	int hex = 0xfe12; /* 0x => hexadécimal */
	int oct = 017;	  /* 0 => octal */
	printf("hexadecimal=%x octal=%o\n",hex,oct);

	float f = 123.0f; /* f littéral de type float, sinon le littéral est un double */
	long l = 123L;	  /* L littéral de type long, sinon le littéral est un int */

	char c = 'E';
	char cAscii = '\47';	  /* littéral en ascii octal */
	char cAsciiHexa = '\x3F'; /* littéral en ascii héxadécimal */
	printf("%c  %c  %4c\n", c, cAscii, cAsciiHexa);

	unsigned int u = 345u; /* u littéral de type unsiged int , sinon le littéral est un int */
	printf("%u\n", u);

	printf("Entrer un caractere ");
	c = getchar(); /* Entrer un caractère au clavier */
	putchar(c);	   /* Afficher un caractère à l'écran */

	printf("\nEntrer un entier et un double ");
	scanf("%d %lf", &i, &hauteur);
	printf("%d %f", i, hauteur);

	fflush(stdin); /* vider le buffer d'entrée */

	/* constante */
	i = MAXIMUM;
	const int n = 123;
	printf("valeur constante");
	/* n=456; Erreur , on ne peut pas modifier une constante */

	/* Transtypage implicite */
	int ti1 = 123;
	double td1 = ti1;
	printf("Transtypage implicite %d %f",ti1,td1);

	/* Transtypage implicite perte précision */
	long l1 = 1234567899;
	long l2 = 1234567898;
	float f1 = l1;
	float f2 = l2;
	long difl12 = l1 - l2;
	float diff12 = f1 - f2;
	printf("%ld  %f\n", difl12, diff12);

	/* Transtypage dépassement de capacité */
	int dep = 136;
	char cep = dep;
	printf("%d  %d\n", dep, cep);

	/* Transtypage explicite */
	int val1 = 123;
	int val2 = 2;
	double div = val1 / val2;
	printf("%f\n", div);
	div = ((double)val1) / ((double)val2);
	printf("%f\n", div);

	/* opérateur arithméthique */
	i = 12;
	int a = 34;
	int somme = a + i;
	printf("%d\n", somme);
	int mod = 45 % 2; /* reste de la division entière */
	printf("%d\n", mod);

	i = 0;
	/* Pré incrémentation */
	int res = ++i; /* i =1 res=1 */
	printf("%d  %d\n", i, res);

	/* Post incrémentation */
	i = 0;
	res = i++; /* i =1 res=0 */

	printf("%d  %d\n", i, res);

	/* Affectation */
	i = 1;
	k = j = i;
	printf("%d %d %d\n", i, j, k);

	/* Affectation élargie */
	i = 12;
	j = 45;
	i += j; /* équivaut à i=i+j */

	/* Boolean en C: vrai !=0 sinon faux ==0 */

	/* Opérateur de comparaison */
	printf("Entrer 2 entiers a comparer ");
	scanf("%d %d\n", &i, &j);
	int cmp = i == j;
	printf("%d %d\n", cmp, !cmp);
	cmp = i == j && i > 10;
	printf("%d\n", cmp);
	cmp = i == j && i++ > 10;
	printf("%d %d\n", cmp, i);

	/* Opérateur binaire */
	unsigned int bin = 0xF3;
	printf("%x\n", bin);
	printf("%x\n", ~bin);
	printf("%x\n", bin & 0x1);
	printf("%x\n", bin | 0x8);
	bin = 4;
	printf("%x\n", bin >> 1);
	printf("%x\n", bin << 1);

	/* sizeof */
	printf("taille d'un int en octets=%d n", sizeof(int));
	printf("taille d'entier=%d , taille d'un double=%d\n", sizeof(i), sizeof(hauteur));

	/* Opérateur séquentiel */
	i = 0;
	a = 2;
	int b = 3;
	if (i++, a += 2, --b, a > b)
	{
		printf("i=%d seq=%d", i, i++, a + b);
	}

	/* Promotion numérique */
	int pr1 = 12;
	long pr2 = 23L;
	long prRes = pr1 + pr2;
	double pr3 = 38.0;
	double prRes2 = pr3 / pr1;
	printf("%ld  %f\n", prRes, prRes2);

	char pr4 = 123;
	char pr5 = 78;
	int prRes3 = pr4 + pr5;
	printf("%d\n", prRes3);

	/* Pointeur */
	int valeur = 1245;
	int *ptr_valeur = &valeur;
	printf("%d %p", valeur, ptr_valeur);
	printf("%d\n", *ptr_valeur);
	*ptr_valeur = 34;
	printf("%d\n", valeur);
	ptr_valeur = NULL;
	printf("%p\n", ptr_valeur);

	/* Condition if */
	int valeurC1, valeurC2;

	printf("Entrer 2 entiers a comparer ");
	scanf("%d %d", &valeurC1, &valeurC2);
	if (valeurC1 > valeurC2)
	{
		printf("val1 est supérieur à val2\n");
	}
	else
	{
		printf("val2 est supérieur à val1\n");
	}

	/* const int LUNDI=1; Erreur on ne pas utiliser de variable const dans un case du switch
		Mais on peut utiliser , une constante définie avec #define */
	
	/* Condition switch */
	int jours;
	printf("Entrer un jour (valeur entre 1 et 7) ");
	scanf("%d", &jours);
	switch (jours)
	{
	case LUNDI:
		printf("Lundi\n");
		break;
	case 6:
	case 7:
		printf("Week end !\n");
		break;
	default:
		printf("Un autre jour\n");
		break;
	}

	/* Opérateur ternaire */
	printf("Entrer un entier (retourne la valeur absolue) ");
	scanf("%d", &valeur);
	int resultat = valeur > 0 ? valeur : -valeur;
	printf("la valeur absolue= %d", resultat);

	/* Boucle while */
	i = 0;
	somme = 0;
	while (i <= 10)
	{
		somme = somme + i;
		i = i + 1;
	}
	printf("somme =%d \n", somme);

	/* Boucle do while */
	i = 0;
	somme = 0;
	do
	{
		somme = somme + i;
		i = i + 1;
	} while (i <= 10);
	printf("somme =%d \n", somme);

	/* Boucle for */
	for (i = 0; i < 10; i = i + 1)
	{
		printf("i =%d \n", i);
	}

	/* Instruction de branchement  */
	/* break */
	for (i = 0; i < 10; i++)
	{
		printf("%d\n", i);
		if (i == 3)
		{
			break;
		}
	}

	for (;;)
	{
		printf("\nEntrer un valeur <100 pour sortir de la boucle ");
		scanf("%d", &i);
		if (i == 1)
		{
			break;
		}
		if (i < 100)
		{
			break;
		}
	}

	/* continue */
	for (i = 0; i < 10; i++)
	{
		if (i == 3)
		{
			continue;
		}
		printf("%d\n", i);
	}

	/* goto */
	for (i = 0; i < 5; i++)
	{
		for (j = 0; j < 3; j++)
		{
			if (i == 2)
			{
				goto END_LOOP;
			}
			printf("%d %d\n", i, j);
		}
	}
END_LOOP: printf("Sortie de la boucle imbriquée");

	/* Tableau à une dimension */
	/* Déclaration */
	double tab[6];

	/* Initialisation à 0 */
	for (i = 0; i < 6; i++)
	{
		tab[i] = 0;
	}
	/* Afficher les valeurs du tableau */
	for (i = 0; i < 6; i++)
	{
		printf("%d [%f]\n", i, tab[i]);
	}

	/* Déclaration et initialisation d'un tableau */
	int tabI[] = {3, 7, 9, 4};

	printf("%d", tabI[1]);
	/* Taille d'un tableau en octets et le nombre d'élément du tableau */
	printf("%d %d", sizeof(tab), sizeof(tab) / sizeof(tab[0]));

	/* Tableau à 2 dimensions */
	/* Déclaration */
	int tab2d[3][4];
	/* Déclaration et initialisation */
	int tab2d2[3][2] = {1, 2, 7, 6, 9, 5};
	/* int tab2d2[3][2]={{1,2},{7,6},{9,5}}; */

	/* Parcours d'un tableau à 2 dimensions*/
	tab2d2[2][1] = -1;
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 2; j++)
		{
			printf("%d\t", tab2d2[i][j]);
		}
		printf("\n");
	}

	/*  */
	printf("%d %d\n", sizeof(tab2d[1]),sizeof(tab2d[1])/sizeof(int));
	return 0;
}