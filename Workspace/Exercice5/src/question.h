#include "reponse.h"

struct question
{
    char intitule[50];
    struct reponse tab_reponse[4];
    int nb_reponse;
};

void init_question(struct question *q, char intitule[]);
int ajouterReponse(struct question *q, struct reponse *r);
void afficher_question(struct question *q);
int test_question(struct question *q,int reponse[],int nb_reponse);