#include <stdio.h>
#include <string.h>
#include "question.h"

void init_question(struct question *q,char intitule[]){
    strcpy(q->intitule,intitule);
    q->nb_reponse=0;
}

int ajouterReponse(struct  question * q,struct reponse *r){
    if(q->nb_reponse>3){
        return 0;
    }
    q->tab_reponse[q->nb_reponse]=*r;
    q->nb_reponse++;
    return 1;
}

void afficher_question(struct  question * q){
    int i=0;
    printf("%s\n",q->intitule);
    for(i=0;i<q->nb_reponse;i++)
    {
        printf("%d - %s\n",i,q->tab_reponse[i].intitule);
    }
}

int test_question(struct question *q,int reponse[],int nb_reponse)
{
    int res=1;
    int i=0;
    for(i=0;i<nb_reponse;i++)
    {
        if(reponse[i]>q->nb_reponse){
            return 0;
        }
        res=res&&q->tab_reponse[reponse[i]].isCorrect;
    }
    return res;
}