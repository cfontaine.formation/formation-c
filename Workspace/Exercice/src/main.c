#include <stdio.h>

#define INTER_MIN -10.0
#define INTER_MAX 30.0
#define EPS 0.00001
#define TAB_SIZE 5

int even(int);
void swap(double *d1, double *d2);
void sort(double tab[], int size_tab);
void afficher(double tab[], int size_tab);

int main(void)
{
	/* Saisir 2 chiffres et afficher le résultat de la somme dans la console */
	/* 	int a, b;
	printf("Saisir deux entier:");
	scanf("%d %d", &a, &b);
	int somme = a + b;
	printf("%d\n\n", somme); */

	/*Créer un programme qui dit si un nombre entier saisit dans la console est pair ou impaire*/
	/* int val;
	printf("Entrer un nombre entier ");
	scanf("%d", &val);
	if (val % 2)
	{
		printf("%d est impaire\n", val);
	}
	else
	{
		printf("%d est paire\n", val);
	} */

	/*Saisir un nombre et tester s'il fait parti de l’intervalle -10.0 exclut et 30.0 inclut */
	/* 	printf("Saisir un nombre ");
	double v;
	scanf("%lf", &v);
	if (v > INTER_MIN && v <= INTER_MAX)
	{
		printf("%d fait parti de l'interval %f a %f", val INTER_MIN, INTER_MAX);
	} */

	/*
	 Faire un programme calculatrice
		Saisir dans la console
		 - un double v1
		 - un caractère opérateur qui a pour valeur valide : + - * /
		 - un double v2

	 Afficher
	 – Le résultat de l’opération
	 – Une message d’erreur si l’opérateur est incorrecte
	 – Une message d’erreur si l’on fait une division par 0
	 */
	/* 	printf("Entrer une opération=");
	double v1, v2;
	char operation;
	scanf("%lf %c %lf", &v1, &operation, &v2);
	switch (operation)
	{
	case '+':
		printf("= %f\n", v1 + v2);
		break;
	case '-':
		printf("= %f\n", v1 - v2);
		break;
	case '*':
		printf("= %f\n", v1 * v2);
		break;
	case '/':
		if (v2 == 0.0)
		{ */
	/* v2>-EPS && v2<EPS */
	/* 			printf("division par 0");
		}
		else
		{
			printf("= %f", v1 / v2);
		}
		break;
	default:
		printf("l'opérateur %c n'est pas valide", operation);
	} */

	/* Faire un programme qui affiche la table de multiplication pour un nombre entier entre 1 et 9
		1 X 4 = 4
		2 X 4 = 8
		…
		9 x 4 = 36
		on exécute le programme tant que la valeur est comprise entre  1 à 9   */
	/* 	int vMul = 1;
	while (vMul > 0 && vMul < 10)
	{
		printf("Entrer une valeur entre 1 et 9\n");
		scanf("%d", &vMul);
		if (vMul > 0 && vMul < 10)
		{
			int i;
			for (i = 1; i < 10; i++)
			{
				printf("%d x %d = %d\n", vMul, i, vMul * i);
			}
		}
	} */

	/* 	Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
		pour 2 lignes et 3 colonnes:
		[ ][ ][ ]
		[ ][ ][ ] */
	/* 	int c, l;
	printf("Nombre de ligne ");
	scanf("%d", &l);
	printf("Nombre de colonne ");
	scanf("%d", &c);
	int i = 0;
	for (i = 0; i < l; i++)
	{
		int j;
		for (j = 0; j < c; j++)
		{
			printf("[ ]");
		}
		printf("\n");
	} */

	/* 	Pour un tableau de 5 entiers
		- saisir au clavier les valeurs du tableau
		- afficher la valeur minimale et  la moyenne*/
	/* 	int tab[TAB_SIZE];
	int i;
	for (i = 0; i < TAB_SIZE; i++)
	{
		printf("tab[%d]=", i);
		scanf("%d", &tab[i]);
	}

	int minimum = tab[0];
	double somme = tab[0];
	for (i = 1; i < TAB_SIZE; i++)
	{
		somme += tab[i];
		if (tab[i] < minimum)
		{
			minimum = tab[i];
		}
	}
	printf("%d %f", minimum, somme / 5); */

	/* printf("%d\n", even(3));
	printf("%d\n", even(2)); */
	double a = 3.0;
	double b = 2.0;
	swap(&a, &b);
	printf("% f %f\n", a, b);

	double tab[] = {10.0, -1.0, 6.0, 4.0, 3.5, -4.0, -100.0};
	afficher(tab, 7);
	sort(tab, 7);
	afficher(tab, 7);
	/* 	printf("%d\n", even(2)); */

	return 0;
}
/* Écrire une méthode even qui prend un entier en paramètre 
   et qui retourne une valeur non nulle si il est paire sinon 0 */
int even(int a)
{
	return a % 2 == 0;
}

void swap(double *d1, double *d2)
{
	double tmp = *d1;
	*d1 = *d2;
	*d2 = tmp;
}

void sort(double tab[], int size_tab)
{
	int loop = 1;
	while (loop)
	{
		loop = 0;
		int i = 0;
		for (i = 0; i < size_tab - 1; i++)
		{
			if (tab[i] > tab[i + 1])
			{
				swap(&tab[i], &tab[i + 1]);
				loop = 1;
			}
		}
	}
}

void afficher(double tab[], int size_tab)
{
	printf("[  ");
	int i;
	for (i = 0; i < size_tab; i++)
	{
		printf("%3.2f  ", tab[i]);
	}
	printf("]\n");
}
