#include <stdio.h>
#include "CompteBancaire.h"

int main(void)
{
	sCompteBancaire cb1;
	initCompteBancaire(&cb1, "Antoine Beretto", "fr-59011", 100.0);
	afficher(&cb1);
	debiterCompteBancaire(&cb1, 1000.0);
	afficher(&cb1);
	crediterCompteBancaire(&cb1, 2500.0);
	afficher(&cb1);
}
