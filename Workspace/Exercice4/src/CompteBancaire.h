struct CompteBancaire
{
    char titulaire[25];
    char iban[8];
    double solde;
};

typedef struct CompteBancaire sCompteBancaire;

void initCompteBancaire(sCompteBancaire *compte, char titulaire[], char iban[], double solde);
void crediterCompteBancaire(sCompteBancaire* compte , double montant);
void debiterCompteBancaire(sCompteBancaire *compte, double montant);
void afficher(const sCompteBancaire *compte);