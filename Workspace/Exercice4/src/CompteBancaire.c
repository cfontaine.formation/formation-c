#include <stdio.h>
#include <string.h>
#include "CompteBancaire.h"

void initCompteBancaire(sCompteBancaire* compte, char titulaire[], char iban[], double solde)
{
    strcpy(compte->iban, iban);
    strcpy(compte->titulaire, titulaire);
    compte->solde = solde;
}

void crediterCompteBancaire(sCompteBancaire* compte, double montant)
{
    if (montant > 0.0)
    {
        compte->solde += montant;
    }
    else
    {
        printf("Le montant n'est pas valide\n");
    }
}
void debiterCompteBancaire(sCompteBancaire* compte, double montant)
{
    if (montant > 0.0)
    {
        compte->solde -= montant;
    }
    else
    {
        printf("Le montant n'est pas valide\n");
    }
}
void afficher(const sCompteBancaire* compte)
{
    printf("______________________________________\n");
    printf("Titulaire:\t%s\n", compte->titulaire);
    printf("Iban:\t\t%s\n", compte->iban);
    printf("Solde:\t\t%f\n", compte->solde);
    printf("______________________________________\n\n");
}