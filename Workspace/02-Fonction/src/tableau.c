#include <stdio.h>
#include "tableau.h"

int var_global_tableau=1230;
static int var_static_tableau=9;

void afficherTableau(int tab[], int size)
{
 
    int i;
    for (i = 0; i < size; i++)
    {
        printf("tab[%d]=%d\n", i, tab[i]);
    }
}

void testExternGlobal(){
    var_global_tableau=124;
    var_static_tableau=123;
    printf("%d",var_static_tableau);
}

