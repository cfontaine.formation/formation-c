#include <stdio.h>
#include <stdarg.h>
#include "tableau.h"

#define TRUE 1 
#define FALSE 0

int var_global = 23;

/*extern int var_global_tableau;*/

int multiplication(int val1, int val2);
void afficher(int);
int *testRetour();
void testParamValeur(int a, int b);
void testParamAdresse(int *adr);

int factorial(int n);
double moyenne(int isDouble, int nbArgs, ...);
void testStatic();

int main(int argc, char *argv[])
{

    int var_locale;
   /*  printf("%d %d\n", var_global, var_locale); */
    /*     int res = multiplication(2, 3);
    afficher(res); */
    /*int* p=testRetour();
    printf("%p\n %d",p ,*p);*/
    /*    int p1 = 10;
    int p2 = 65;
    printf("%d %d\n", p1, p2);
    testParamValeur(p1, p2);
    printf("%d %d\n", p1, p2);
    testParamAdresse(&p1);
    printf("%d\n", p1);*/
    int tab[] = {1, 5, 7, 8};
    afficherTableau(tab, 4); 
    testExternGlobal();
    /*  factorial(3); */
    /*  */
    /*     int i;
    printf("Nombre de parametre=%d\n",argc);
    for(i=0;i<argc;i++)
    {
        printf("%s\n",argv[i]);
    } */
/*     printf("%f\n", moyenne(FALSE, 3, 3, 4, 5));

    int a = 3;
    int b = 34;
    printf("%d\n", b);
    if (a == 3)
    {
        int b = 0;
        printf("%d %d\n", a, b);
    }
    printf("%d\n", b); */
    testStatic();
    testStatic();
    testStatic();

    /* printf("%d\n",var_global_tableau); */

    return 0;
}

int multiplication(int v1, int v2)
{
    printf("int\n");
    int mul = v1 * v2;
    return mul;
}

void afficher(int a)
{
    printf("%d\n", a);
}

int *testRetour()
{
    int a = 23;
    int *ptr = &a;
    return ptr;
}

void testParamValeur(int a, int b)
{
    printf("%d %d\n", a, b);
    a = 29;
    printf("%d %d\n", a, b);
    /*  ... */
}

void testParamAdresse(int *adr)
{
    (*adr)++;
}



int factorial(int n)
{
    if (n <= 1) /* condition de sortie */
    {
        return 1;
    }
    else
    {
        return factorial(n - 1) * n;
    }
}

double moyenne(int isDouble, int nbArgs, ...)
{
    va_list arg_list;
    va_start(arg_list, nbArgs);
    int i;
    double somme = 0;
    for (i = 0; i < nbArgs; i++)
    {
        if (isDouble == 0)
        {
            somme += va_arg(arg_list, int);
        }
        else
        {
            somme += va_arg(arg_list, double);
        }
        /* printf("%d-> %d\n",i,va_arg(arg_list,int)); */
    }
    va_end(arg_list);
    return somme / nbArgs;
}

void testStatic(){
    int var_auto=0;
    static int cpt=34;
    register char c=12;
   /* char ptr_char=&c;*/
    cpt++;
    var_auto++;
    printf("%d %d\n",cpt,var_auto);
}
void testExtern()
{
    extern int var_global_tableau;
    printf("%d",var_global_tableau);
}